-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-11-2014 a las 09:27:03
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `proyectobd`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `Donante_palabra`(IN `donante` VARCHAR(60))
    NO SQL
select palabra.palabra AS "palabra asociada" from donante, donante_evento, evento, tweet_evento, tweet, palabra_tweet, palabra
where
palabra.id_palabra = palabra_tweet.id_palabra and
palabra_tweet.id_tweet = tweet.id_tweet and
tweet.id_tweet = tweet_evento.id_tweet and
tweet_evento.id_evento = evento.id_evento and
evento.id_evento = donante_evento.id_evento and
donante_evento.id_donante = donante.id_donante and
donante.nombre_donante = donante$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Eliminar Donante`(IN `id` INT)
    NO SQL
begin
delete from donante where donante.id_donante = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Eliminar Evento`(IN `id` INT)
    NO SQL
begin
delete from evento where evento.id_evento = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Eliminar Hashtag`(IN `id` INT)
    NO SQL
begin
delete from hashtag where hashtag.id_hashtag = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Eliminar Palabra`(IN `id` INT)
    NO SQL
begin
delete from palabra where palabra.id_palabra = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Eliminar Persona Publica`(IN `id` INT)
    NO SQL
begin
delete from persona_publica where persona_publica.id_persona = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Eliminar Twittero`(IN `usuario` VARCHAR(20))
    NO SQL
begin
delete from twittero where twittero.usuario_twittero = usuario;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Hashtag_que_tiene_un_tweet`(IN `tweet` VARCHAR(60))
    NO SQL
select hashtag.hashtag AS "Hashtag" from hashtag, hashtag_donante, donante, donante_evento, evento, tweet_evento, tweet
where
hashtag.id_hashtag = hashtag_donante.id_hashtag and
donante.id_donante = hashtag_donante.id_donante and
donante_evento.id_donante = donante.id_donante and
evento.id_evento = donante_evento.id_evento and
tweet_evento.id_evento = evento.id_evento and
tweet.id_tweet = tweet_evento.id_tweet and
tweet.tweet= tweet$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Ingresar Donante`(IN `id` INT, IN `nombre_donador` VARCHAR(60), IN `monto_donado` INT, IN `clasificacion` VARCHAR(60))
    NO SQL
begin
insert into donante(`id_donante`,`nombre_donante`,`monto_donacion`,`clasificacion_donante`)values(id,nombre_donador,monto_donado,clasificacion);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Ingresar Evento`(IN `Id` INT(60), IN `Nombre_evento` VARCHAR(60), IN `Fecha` DATE, IN `Lugar` VARCHAR(60), IN `Usuario_Twitter` VARCHAR(60))
    NO SQL
begin
insert into evento(`id_evento`,`nombre_evento`,`fecha_evento`,`lugar_evento`,`usuario_twitter`)values(id,nombre_evento,now(),lugar, usuario_twitter);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Ingresar Hashtag`(IN `id` INT, IN `hashtag` VARCHAR(60))
    NO SQL
begin
insert into hashtag(`id_hashtag`,`hashtag`)values(id,hashtag);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Ingresar Palabra`(IN `id` INT, IN `palabra` VARCHAR(60), IN `clasificacion` VARCHAR(60))
    NO SQL
begin
insert into palabra(`id_palabra`,`palabra`,`clasificacion_palabra`)values(id, palabra, clasificacion);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Ingresar Persona`(IN `id` INT, IN `Nombre_usuario` VARCHAR(60), IN `Edad` INT, IN `Nombre` VARCHAR(60), IN `Tipo_persona` VARCHAR(60))
    NO SQL
begin
insert into persona_publica(`id_persona`,`nombre_usuario`,`edad_persona`,`nombre_persona`,`tipo_persona`)values(id,nombre_usuario,edad,nombre, tipo_persona);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `IngresarTwittero`(IN `usuario` VARCHAR(30))
    NO SQL
begin
insert into 
twittero(`id_twittero`,`usuario_twittero`)values(`NULL`,usuario_twittero);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertaDesdeTwitter`(IN `tweet` VARCHAR(140), IN `fecha` DATE, IN `hora` TIME, IN `cuenta` VARCHAR(20))
    NO SQL
begin
insert into twittero(`id_twittero`,`usuario_twittero`)values(`NULL`,usuario_twittero);
insert into tweet(`Tweet`,`fecha_publicacion`,`hora_publicacion`)values(tweet,fecha,hora);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Personaje_por_evento`(IN `personaje` VARCHAR(60))
    NO SQL
select evento.nombre_evento, evento.lugar_evento AS "Lugar" from evento, persona_evento, persona_publica
where
evento.id_evento = persona_evento.id_evento and
persona_publica.id_persona = persona_evento.id_persona and
persona_publica.nombre_persona = personaje$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `donante`
--

CREATE TABLE IF NOT EXISTS `donante` (
`id_donante` int(11) NOT NULL,
  `nombre_donante` varchar(45) NOT NULL,
  `monto_donacion` int(11) NOT NULL,
  `clasificacion_donante` varchar(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=151 ;

--
-- Volcado de datos para la tabla `donante`
--

INSERT INTO `donante` (`id_donante`, `nombre_donante`, `monto_donacion`, `clasificacion_donante`) VALUES
(3, 'Johnson S.A.', 100000000, 'EM'),
(4, 'Almacen Juanito', 5000000, 'EM'),
(5, 'Leonardo Farkas', 500000000, 'PN'),
(6, 'Maria Constanza', 40000, 'PN'),
(7, 'Aceite Belmont', 100000000, 'EG'),
(8, 'Watts', 101000000, 'EG'),
(9, 'Banco de Chile', 1000524503, 'EG'),
(10, 'Bilz y Pap', 352253389, 'EG'),
(11, 'CLARO', 209979000, 'EG'),
(12, 'Falabella', 5000000, 'EG');

--
-- Disparadores `donante`
--
DELIMITER //
CREATE TRIGGER `Borrar Donante` BEFORE DELETE ON `donante`
 FOR EACH ROW delete from donante_evento where id_donante = old.id_donante
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `donante_evento`
--

CREATE TABLE IF NOT EXISTS `donante_evento` (
  `id_donante` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `donante_evento`
--

INSERT INTO `donante_evento` (`id_donante`, `id_evento`) VALUES
(1, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE IF NOT EXISTS `evento` (
`id_evento` int(11) NOT NULL,
  `nombre_evento` varchar(45) NOT NULL,
  `fecha_evento` date NOT NULL,
  `lugar_evento` varchar(45) NOT NULL,
  `usuario_twitter` varchar(45) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`id_evento`, `nombre_evento`, `fecha_evento`, `lugar_evento`, `usuario_twitter`) VALUES
(1, 'Teleton Chile', '2014-11-28', 'Teatro Teletón', '@Teleton'),
(2, 'Vedeton', '2014-11-29', 'Teatro Teleton', '@Teleton'),
(3, 'Mister Teleton', '2014-11-29', 'Teatro Teleton', '@Teleton'),
(4, 'Tallarinata Magistral', '2014-11-25', 'USACH', '@USACH'),
(5, 'Maraton Teleton', '2014-11-25', 'Republica hasta plaza italia', '@SomosTodos'),
(6, 'Innovaton', '2014-11-05', 'Casa matriz Santiago', '@Teletón'),
(7, 'Cierre Teleton', '2014-11-29', 'Estadio nacional', '@Cony'),
(8, 'Gira Teletón', '2014-10-13', 'Iquique', '@Teleton'),
(9, 'Gira Teletón', '2014-11-12', 'Antofagasta', '@somosTodos'),
(10, 'Programa infantil', '2014-11-29', 'Teatro Teleton', '@SomosTodos'),
(11, 'Futbol solidario', '2014-11-25', 'Teatro Caupolican', '@IvanZamorano'),
(12, 'Gira Teletón', '2014-10-14', 'Valparaiso', '@TeletonValpo'),
(13, 'Humor Teletón', '2014-11-29', 'Teatro Teletón', '@CheCopete'),
(14, 'Presentacion Romeo Santos', '2014-11-29', 'Estadio Nacional', '@RomeoSantos'),
(15, 'Presentación La Ley', '2014-11-29', 'Estadio Nacional', '@LaLey'),
(16, 'Presentación Jorge Gonzalez', '2014-11-29', 'Estadio Nacional', '@JorgeGonzalez'),
(50, 'Corridas', '2014-11-26', 'Alameda', '@Pablete'),
(51, 'Mister Piernas', '2014-11-26', 'Teatro Teleton', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hashtag`
--

CREATE TABLE IF NOT EXISTS `hashtag` (
`id_hashtag` int(11) NOT NULL,
  `hashtag` varchar(45) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Volcado de datos para la tabla `hashtag`
--

INSERT INTO `hashtag` (`id_hashtag`, `hashtag`) VALUES
(1, '#TeletonLaLleva'),
(2, '#TeletonPuroToyo'),
(3, '#BuenaClaro'),
(4, '#TeletonNiunBrillo'),
(5, '#AranedaLaLleva'),
(6, '#TodosSomosTeleton'),
(7, '#DonFranciscoCorleone'),
(8, '#LaMafiaTelevisiva'),
(9, '#LadronesQlCencosud'),
(10, '#DevuelvanNuestroDineroTeletonCTM'),
(11, '#TeletonChile'),
(12, '#Teleton'),
(13, '#DonFrancisco'),
(14, '#MarioKreutzberger'),
(15, '#Araneda'),
(16, '#Viñuela'),
(17, '#Tonka'),
(18, '#EvaGomez'),
(19, '#TeletonSomoTodos'),
(20, '#ConTodoElCorazon'),
(21, '#Teleton2014'),
(22, '#24500-03'),
(23, '#TeletonPuroRobo'),
(24, '#TeletonQL'),
(25, '#Ricoooo'),
(26, '#LaNocheEnTeleton'),
(27, '#KikeMorande'),
(28, '#Vodanovic'),
(29, '#TonkaRica'),
(30, '#RomeoSantos'),
(31, ' #Colun'),
(32, '#Copec'),
(33, '#LAN'),
(34, '#OMO'),
(35, '#Tapsin'),
(36, '#BancoDeChile'),
(37, '#CANNES #GUAU'),
(38, '#Magistral'),
(39, '#Cristal #TengoSed'),
(40, '#Colun #LaMagiaDelSur');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hashtag_donante`
--

CREATE TABLE IF NOT EXISTS `hashtag_donante` (
  `id_hashtag` int(11) NOT NULL,
  `id_donante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `hashtag_donante`
--

INSERT INTO `hashtag_donante` (`id_hashtag`, `id_donante`) VALUES
(2, 8),
(2, 9),
(3, 11),
(9, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabra`
--

CREATE TABLE IF NOT EXISTS `palabra` (
`id_palabra` int(11) NOT NULL,
  `palabra` varchar(45) NOT NULL,
  `clasificacion_palabra` varchar(45) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

--
-- Volcado de datos para la tabla `palabra`
--

INSERT INTO `palabra` (`id_palabra`, `palabra`, `clasificacion_palabra`) VALUES
(2, 'ctm', 'Negativa'),
(3, 'ladrones', 'Negativa'),
(4, 'ladron', 'Negativa'),
(5, 'bien', 'Positiva'),
(6, 'buena', 'Positiva'),
(7, 'qla', 'Negativo'),
(8, 'qlo', 'Negativa'),
(9, 'culiao', 'Negativa'),
(10, 'Culiaos', 'Negativa'),
(11, 'bakan', 'Positiva'),
(12, 'bakanisimo', 'Positiva'),
(13, 'Excelente', 'Positiva'),
(14, 'genial', 'Positiva'),
(15, 'bkn', 'Positiva'),
(16, 'hermoso', 'Positiva'),
(17, 'hermosos', 'Positiva'),
(18, 'Callampa', 'Negativa'),
(19, 'pico', 'Negativa'),
(100, 'Rechuchetumadre', 'Negativa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabra_tweet`
--

CREATE TABLE IF NOT EXISTS `palabra_tweet` (
  `id_palabra` int(11) NOT NULL,
  `id_tweet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `palabra_tweet`
--

INSERT INTO `palabra_tweet` (`id_palabra`, `id_tweet`) VALUES
(0, 2),
(19, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_evento`
--

CREATE TABLE IF NOT EXISTS `persona_evento` (
  `id_persona` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona_evento`
--

INSERT INTO `persona_evento` (`id_persona`, `id_evento`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_publica`
--

CREATE TABLE IF NOT EXISTS `persona_publica` (
`id_persona` int(11) NOT NULL,
  `nombre_usuario` varchar(45) NOT NULL,
  `edad_persona` int(11) NOT NULL,
  `nombre_persona` varchar(45) NOT NULL,
  `tipo_persona` varchar(45) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

--
-- Volcado de datos para la tabla `persona_publica`
--

INSERT INTO `persona_publica` (`id_persona`, `nombre_usuario`, `edad_persona`, `nombre_persona`, `tipo_persona`) VALUES
(1, '@RafaAraneda', 44, 'Rafael Araneda', 'ANIMADOR'),
(2, '@TonkaTP', 39, 'Tonka Tomicic', 'ANIMADOR'),
(3, '@clarochile_cl', 12, 'Telefonía Claro', 'EMPRESA'),
(4, '@ricky_martin', 45, 'Ricky Martin', 'ARTISTA'),
(5, '@FulvioRossi', 46, 'Fulvio Rossi', 'Politico'),
(6, '@JorgeCastro', 35, '@JorgeCastro_', 'Politico'),
(7, '@PepeAuth', 56, 'Pepe Auth', 'Politico'),
(8, '@Coloma', 80, 'Coloma', 'Poitica'),
(9, '@MichelleBachelet', 60, 'Michelle Bachelet', 'Politica'),
(10, '@MarianaMarino', 30, 'Mariana Marino', 'Modelo'),
(11, '@BamBamMorais', 35, 'Bam Bam Morais', 'Animador'),
(12, '@MarceloMarrochino', 32, 'Marcelo Marrochino', 'Modelo'),
(13, '@Kramer', 30, 'Stefan Kramer', 'Humorista'),
(100, '@Constanza', 25, 'Cony', 'Cantante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tweet`
--

CREATE TABLE IF NOT EXISTS `tweet` (
`id_tweet` int(11) NOT NULL,
  `Tweet` varchar(140) DEFAULT NULL,
  `cantidad_retweet` int(11) NOT NULL,
  `fecha_publicacion` date NOT NULL,
  `hora_publicacion` time NOT NULL,
  `Clasificacion` varchar(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `tweet`
--

INSERT INTO `tweet` (`id_tweet`, `Tweet`, `cantidad_retweet`, `fecha_publicacion`, `hora_publicacion`, `Clasificacion`) VALUES
(1, '@TELETON vale callampa', 4, '2014-11-20', '14:23:39', 'M'),
(2, 'yo me pongo con la @teleton', 2, '2014-11-04', '03:28:35', 'N'),
(3, '@RafaAraneda se luce en la @Teleton', 0, '2014-11-02', '17:11:54', 'B');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tweet_evento`
--

CREATE TABLE IF NOT EXISTS `tweet_evento` (
  `id_tweet` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tweet_evento`
--

INSERT INTO `tweet_evento` (`id_tweet`, `id_evento`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tweet_twittero`
--

CREATE TABLE IF NOT EXISTS `tweet_twittero` (
  `id_tweet` int(11) NOT NULL,
  `id_twittero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tweet_twittero`
--

INSERT INTO `tweet_twittero` (`id_tweet`, `id_twittero`) VALUES
(1, 1),
(1, 151),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `twittero`
--

CREATE TABLE IF NOT EXISTS `twittero` (
`id_twittero` int(11) NOT NULL,
  `usuario_twittero` varchar(45) NOT NULL,
  `comentariosPositivos` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=556 ;

--
-- Volcado de datos para la tabla `twittero`
--

INSERT INTO `twittero` (`id_twittero`, `usuario_twittero`, `comentariosPositivos`) VALUES
(2, '@ConyFlores', 0),
(3, '@OsvaldoParra', 0),
(4, '@PedritoSureño', 0),
(6, '@EllenPage', 0),
(7, '@Te_gusta_poco', 0),
(8, '@Benito_lindo', 0),
(9, '@Fabian_A', 0),
(10, '@USACH', 0),
(11, '@Andrea_Molina', 0),
(12, '@Salfate', 0),
(13, '@DonFrancisco', 0),
(14, '@JoseMiguelViñuela', 0),
(15, '@Danitza', 0),
(16, '@Corleone', 0),
(150, '@Conistica', 0),
(151, '@Conyx', 0);

--
-- Disparadores `twittero`
--
DELIMITER //
CREATE TRIGGER `Insertar Twiitero` AFTER INSERT ON `twittero`
 FOR EACH ROW insert into tweet_twittero(`id_tweet`,`id_twittero`)values(now(),new.id_twittero)
//
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `donante`
--
ALTER TABLE `donante`
 ADD PRIMARY KEY (`id_donante`);

--
-- Indices de la tabla `donante_evento`
--
ALTER TABLE `donante_evento`
 ADD PRIMARY KEY (`id_donante`,`id_evento`);

--
-- Indices de la tabla `evento`
--
ALTER TABLE `evento`
 ADD PRIMARY KEY (`id_evento`);

--
-- Indices de la tabla `hashtag`
--
ALTER TABLE `hashtag`
 ADD PRIMARY KEY (`id_hashtag`);

--
-- Indices de la tabla `hashtag_donante`
--
ALTER TABLE `hashtag_donante`
 ADD PRIMARY KEY (`id_hashtag`,`id_donante`);

--
-- Indices de la tabla `palabra`
--
ALTER TABLE `palabra`
 ADD PRIMARY KEY (`id_palabra`);

--
-- Indices de la tabla `palabra_tweet`
--
ALTER TABLE `palabra_tweet`
 ADD PRIMARY KEY (`id_palabra`,`id_tweet`);

--
-- Indices de la tabla `persona_evento`
--
ALTER TABLE `persona_evento`
 ADD PRIMARY KEY (`id_persona`,`id_evento`);

--
-- Indices de la tabla `persona_publica`
--
ALTER TABLE `persona_publica`
 ADD PRIMARY KEY (`id_persona`);

--
-- Indices de la tabla `tweet`
--
ALTER TABLE `tweet`
 ADD PRIMARY KEY (`id_tweet`);

--
-- Indices de la tabla `tweet_evento`
--
ALTER TABLE `tweet_evento`
 ADD PRIMARY KEY (`id_tweet`,`id_evento`);

--
-- Indices de la tabla `tweet_twittero`
--
ALTER TABLE `tweet_twittero`
 ADD PRIMARY KEY (`id_tweet`,`id_twittero`);

--
-- Indices de la tabla `twittero`
--
ALTER TABLE `twittero`
 ADD PRIMARY KEY (`id_twittero`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `donante`
--
ALTER TABLE `donante`
MODIFY `id_donante` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT de la tabla `evento`
--
ALTER TABLE `evento`
MODIFY `id_evento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT de la tabla `hashtag`
--
ALTER TABLE `hashtag`
MODIFY `id_hashtag` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `palabra`
--
ALTER TABLE `palabra`
MODIFY `id_palabra` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT de la tabla `persona_publica`
--
ALTER TABLE `persona_publica`
MODIFY `id_persona` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT de la tabla `tweet`
--
ALTER TABLE `tweet`
MODIFY `id_tweet` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `twittero`
--
ALTER TABLE `twittero`
MODIFY `id_twittero` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=556;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;